const express = require('express')

// controllers
const adminCtrl = require('../controllers/admin.controller')
const insurCtrl = require('../controllers/insurance.controller')

// validations
const authValid = require('../validations/auth.validation')
const insurValid = require('../validations/insurance.validation')

// middlewares
const adminAuth = require('../middleware/auth.middleware')

// router instance
const router = express.Router()

// Authentication
router.route('/signup').post(adminAuth, authValid.signup, adminCtrl.signup)
router.route('/signin').post(adminAuth, authValid.signin, adminCtrl.signin)
router.route('/userinfo').get(adminAuth, adminCtrl.getAdminInfo)

// Controls
router.route('/insurance')
    .post(adminAuth, insurValid.AddInsurance, insurCtrl.addInsurance)
    .get(adminAuth, insurCtrl.getInsurList)

router.route('/patient')
    .post(adminAuth, adminCtrl.addPatient)
    .get(adminAuth, adminCtrl.getPatient)

module.exports = router
