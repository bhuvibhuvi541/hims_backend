const expressAsyncHandler = require('express-async-handler')

const Insurance = require('../models/insurance')

exports.addInsurance = expressAsyncHandler(async (req, res) => {
  try {
    const { body } = req

    let doc = {
      name: body.name,
      headQuaters: body.headQuaters,
      joinedHospitalOn: body.joinedHospitalOn,
    }

    let checkCompany = await Insurance.findOne({ name: new RegExp(doc.name, 'i') })

    if (checkCompany) {
      return res
        .status(400)
        .json({ success: false, message: 'Already a company exists with similar name' })
    }

    let newCompany = new Insurance(doc)
    await newCompany.save()

    return res.json({ success: true, result: newCompany })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: 'Something went wrong' })
  }
})

exports.getInsurList = expressAsyncHandler(async (req, res) => {
  try {
    let insurList = await Insurance.find({})
    return res.json({ success: true, result: insurList })
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: 'Something went wrong' })
  }
})
