const expressAsyncHandler = require('express-async-handler')
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Admin = require('../models/admin')
const Patient = require('../models/patient')

exports.signup = expressAsyncHandler(async (req, res) => {
    try {
        const { body } = req

        let doc = {
            name: body.name,
            email: body.email,
            password: bcryptjs.hashSync(body.password, 10),
        }

        let admin = new Admin(doc)
        await admin.save()

        return res.json({
            success: true,
            message: 'Signup successfull',
            user: admin,
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: 'Something went wrong' })
    }
})

exports.signin = expressAsyncHandler(async (req, res) => {
    try {
        const { body } = req

        let checkUser = await Admin.findOne({ email: body.email })

        if (!checkUser) {
            return res.status(400).json({ success: false, message: 'Admin not found' })
        }

        let comparison = bcryptjs.compareSync(body.password, checkUser.password)

        if (!comparison) {
            return res.status(400).json({ success: false, message: 'Incorrect password' })
        }

        let token = jwt.sign({ id: checkUser._id }, process.env.JWT_SECRET)

        return res.json({
            success: true,
            message: 'Sign in successfull',
            token: `Bearer ${token}`,
            user: checkUser,
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: 'Something went wrong' })
    }
})

exports.getAdminInfo = expressAsyncHandler(async (req, res) => {
    try {
        let admin = await Admin.findOne({ _id: req.user._id })
        return res.json({ success: true, userId: admin._id, user: admin })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: 'Something went wrong' })
    }
})

exports.addPatient = expressAsyncHandler(async (req, res) => {
    try {
        const { body } = req
        const patient = new Patient({ ...body })
        await patient.save()
        return res.json({ success: true, message: 'Patient details added successfully' })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: 'Something went wrong' })
    }
})

exports.getPatient = expressAsyncHandler(async (req, res) => {
    try {
        const { patientId } = req.query

        const patient = await Patient.findOne({ aadhaar: new RegExp(patientId, 'i') })

        if (!patient) {
            return res.json({ success: false, message: 'Patient not found' })
        }

        return res.json({ success: true, patient, message: 'Patient found successfully' })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: 'Something went wrong' })
    }
})
