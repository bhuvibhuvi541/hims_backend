const mongoose = require('mongoose')

const AddressSchema = new mongoose.Schema({
  door: {
    type: String,
    required: true,
  },
  street: {
    type: String,
    required: true,
  },
  town: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  district: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
    enum: ['Urban', 'Sub-Urban', 'Rural'],
  },
})

const InsuranceSchema = new mongoose.Schema({
  company: {
    type: String,
    required: true,
    // ref: 'insurance',
  },
  startedon: {
    type: Date,
    required: true,
  },
  expires: {
    type: Date,
    required: true,
  },
  coverage: {
    type: String,
    required: true,
  },
})

const PatientSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    address: {
      type: AddressSchema,
      required: true,
    },
    isAlive: {
      type: Boolean,
      default: true,
    },
    diedOn: {
      type: Date,
      required: false,
    },
    deathCertificateIssueDate: {
      type: Date,
      required: false,
    },
    deathClaim: {
      type: String,
      required: false,
    },
    phoneNumber: {
      type: String,
      default: '',
    },
    aadhaar: {
      type: String,
      default: '',
    },
    insurance: {
      type: InsuranceSchema,
      default: {},
      ref: 'insurance',
    },
  },
  {
    timestamps: true,
  },
)

module.exports = mongoose.model('patient', PatientSchema, 'patient')
