const mongoose = require('mongoose')

const HistorySchema = new mongoose.Schema(
  {
    patientId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: 'patient',
    },
    totalBill: {
      type: String,
      required: true,
    },
    dateOfAdmission: {
      type: Date,
      required: true,
    },
    isInsuranceClaim: {
      type: Boolean,
      required: true,
    },
    insuranceCompany: {
      type: mongoose.Types.ObjectId,
      required: false,
    },
    insuranceAmount: {
      type: Number,
      default: 0,
    },
    claimedAmount: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  },
)

module.exports = mongoose.model('history', HistorySchema, 'history')
