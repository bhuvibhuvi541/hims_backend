const isEmpty = require('../lib/isEmpty')

exports.AddInsurance = (req, res, next) => {
  try {
    const { body } = req
    const errors = {}

    if (isEmpty(body.name)) {
      errors.name = 'Name of the company cannot be empty'
    }

    if (isEmpty(body.headQuaters)) {
      errors.headQuaters = 'Branch name cannot be empty'
    }

    if (isEmpty(body.joinedHospitalOn)) {
      errors.joinedHospitalOn = 'Join date cannot be empty'
    }

    if (!isEmpty(errors)) {
      return res.status(400).json({ success: false, message: 'Validation errors', errors })
    }

    next()
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: 'Something went wrong' })
  }
}
