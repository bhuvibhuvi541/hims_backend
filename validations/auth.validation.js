const isEmpty = require('../lib/isEmpty')

exports.signup = (req, res, next) => {
  try {
    const { body } = req
    const errors = {}

    if (isEmpty(body.name)) {
      errors.name = 'Name cannot be empty'
    }

    if (isEmpty(body.email)) {
      errors.email = 'Email cannot be empty'
    }

    if (isEmpty(body.password)) {
      errors.password = 'Password cannot be empty'
    }

    if (!isEmpty(errors)) {
      return res.status(400).json({ success: false, message: 'Validate errors', errors })
    }
    next()
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: 'Something went wrong' })
  }
}

exports.signin = (req, res, next) => {
  try {
    const { body } = req
    const errors = {}

    if (isEmpty(body.email)) {
      errors.email = 'Email cannot be empty'
    }

    if (isEmpty(body.password)) {
      errors.password = 'Password cannot be empty'
    }

    if (!isEmpty(errors)) {
      return res.status(400).json({ success: false, message: 'Validate errors', errors })
    }
    next()
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: 'Something went wrong' })
  }
}
