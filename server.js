console.log(`\u001B[31m${process.env.RUNTIME_MODE} Server`)

// dot-env config
require('dotenv').config()

// imports - packages
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

// imports - routes
const adminroute = require('./routes/admin')

// app instance
const app = express()

// Essentials
app.use(
    cors({
        methods: ['GET', 'POST', 'PUT', 'DELETE'], // methods allowed to perform in this server
        origin: '*', // This accepts request from any point-of-origin, change this to your corresponding frontend url, eg: ["http://localhost:3000"]
    }),
)

app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())

app.use(morgan('dev'))

//Route Declaration
app.use('/api', adminroute) // directs requests which starts with [host]/api/*

mongoose.set('strictQuery', false) // used this for upcoming mongoose update
mongoose.connect(process.env.MONGO_URI, (err) => {
    if (err) {
        console.log(err)
        process.exit()
    }
    console.log('\u001B[35mConnected to HIMS database')
    app.listen(process.env.PORT, (err) => {
        if (err) {
            console.log(err)
            process.exit()
        }
        console.log(`\u001B[34mServer runs on port ${process.env.PORT}`)
    })
})
