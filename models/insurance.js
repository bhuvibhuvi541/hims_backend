const mongoose = require('mongoose')

const InsuranceSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    headQuaters: {
      type: String,
      required: true,
    },
    insuranceIssued: {
      type: Number,
      default: 0,
    },
    totalClaims: {
      type: Number,
      default: 0,
    },
    totalAmountClaimed: {
      type: Number,
      default: 0,
    },
    joinedHospitalOn: {
      type: Date,
      required: true,
    },
  },
  {
    timestamps: true,
  },
)

module.exports = mongoose.model('insurance', InsuranceSchema, 'insurance')
